#!/bin/bash

source /home/$USER/auto_djyx_tractor/install/setup.sh

gnome-terminal --tab -- bash -c "roscore" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch runtime_manager runtime_manager.launch" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch lslidar_driver lslidar_c16.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins map.launch" 
sleep 10
gnome-terminal --tab -- bash -c "roslaunch ins tf_map.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization_waypoint.launch" 
wait
exit 0
