#!/bin/bash

source /home/$USER/auto_djyx_tractor/install/setup.sh

gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization_lx.launch" 
sleep 1
gnome-terminal --tab -- bash -c "roslaunch ins controller.launch"
sleep 1
gnome-terminal --tab -- bash -c "rosnode kill /can_status_translator /op_global_planner /pose_relay /vel_relay;rosnode kill /myusbcan"
sleep 2
gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization_lx.launch" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins controller.launch"
wait
exit 0
