#!/bin/bash

# 判断主目录下是否存在bag.bag
if [ -e ~/bag.bag ]; then
  source /home/$USER/auto_djyx_tractor/install/setup.sh

  gnome-terminal --tab -- bash -c "roscore" 
  sleep 5
  gnome-terminal --tab -- bash -c "roslaunch hdl_graph_slam hdl_graph_slam_outdoor.launch"
  sleep 5
  gnome-terminal --tab -- bash -c "rosbag play /home/$USER/bag.bag"
  wait
  exit 0
else
  gnome-terminal --tab -- bash -c "  ps -ef|grep ros|awk '{print $2}'|xargs kill -9"
fi
