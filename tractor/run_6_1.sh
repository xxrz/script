#!/bin/bash
source /home/$USER/auto_djyx_tractor/install/setup.sh

MAP_PATH="tf_map_1"

gnome-terminal --tab -- bash -c "roslaunch ins tf_map_sx.launch"
sleep 1
gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization.launch" 
gnome-terminal --tab -- bash -c "rosnode kill /ndt_config /world_to_map;rosnode kill /ndt_matching"
sleep 2
gnome-terminal --tab -- bash -c "roslaunch ins $MAP_PATH.launch"
sleep 2
gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization.launch" 
exit 0

