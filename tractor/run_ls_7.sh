#!/bin/bash

MAP_PATH="/home/$USER/"
MAP_PATH1="bag.bag"

source /home/$USER/auto_djyx_tractor/install/setup.sh

gnome-terminal --tab -- bash -c "roscore" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch lslidar_driver lslidar_c16.launch"
sleep 3
gnome-terminal --tab -- bash -c "rosbag record -o "$MAP_PATH$MAP_PATH1" /points_raw"
wait
exit 0
