import subprocess
import os

# 定义命令和输出文件路径
command = "rostopic echo /move_base_simple/goal"
output_file = os.path.expanduser("~/output.txt")

# 在运行命令之前检查并删除输出文件
if os.path.exists(output_file):
    os.remove(output_file)

# 执行命令并将输出重定向到文件
with open(output_file, "w+") as f:
    process = subprocess.Popen(command, shell=True, stdout=f, stderr=subprocess.PIPE)

# 读取错误输出
error = process.stderr.read()

