#import tkinter as tk
import sys
import os
import subprocess

def run_script(script):
    # 获取临时目录路径
    main_dir = os.path.expanduser("~")

    # 拼接脚本文件路径
    script_path = os.path.join(main_dir,"./.sh/script/tool/agv/", script)


    # 使用subprocess模块执行启动命令
    subprocess.call(['python3', script_path])

def show_popup():
    #root = tk.Tk()
    #root.withdraw()  # 隐藏根窗口

    # 指定要启动的脚本文件名
    scripts = ['tr.py']

    # 逐个运行脚本
    for script in scripts:
        run_script(script)

show_popup()

