#!/usr/bin/env python3
import os
#from tkinter import messagebox

#messagebox.showinfo("注意", "请将4个坐标放进主目录output.txt中，每组坐标都是相同的，准备完点击确定进行生成!!!")
# 读取 output.txt 文件内容
with open(os.path.expanduser("~/output.txt"), "r") as f:
    content = f.read()

# 提取 x 和 y 的坐标值
coordinates = []
start = content.find("position:")
while start != -1:
    x_val = ""
    y_val = ""

    # 提取 x 和 y 的坐标值
    x_index = content.find("x:", start) + 2
    next_line_index = content.find("\n", x_index)
    if next_line_index != -1:
        x_val = content[x_index:next_line_index].strip()

    y_index = content.find("y:", next_line_index) + 2
    next_line_index = content.find("\n", y_index)
    if next_line_index != -1:
        y_val = content[y_index:next_line_index].strip()

    if x_val and y_val:
        coordinates.append((x_val, y_val))

    start = content.find("position:", next_line_index)

# 构造需要填入的代码
code_1 = f"if(box.pose.position.x > {min(float(coordinates[0][0]), float(coordinates[1][0]))} && box.pose.position.x < {max(float(coordinates[0][0]), float(coordinates[1][0]))} && box.pose.position.y > {min(float(coordinates[0][1]), float(coordinates[1][1]))} && box.pose.position.y < {max(float(coordinates[0][1]), float(coordinates[1][1]))})\n\tAGV_STOP.data = True"
code_2 = f"if(v_x>{min(float(coordinates[2][0]), float(coordinates[3][0]))}&&v_x<{max(float(coordinates[2][0]), float(coordinates[3][0]))}&&v_y>{min(float(coordinates[2][1]), float(coordinates[3][1]))}&&v_y<{max(float(coordinates[2][1]), float(coordinates[3][1]))})\n\tv_in = true;"

# 写入到 1.md 文件中
with open(os.path.expanduser("~/1.md"), "w") as f:
    f.write(code_1)
    f.write("\n")
    f.write("\n")
    f.write("\n")
    f.write("\n")
    f.write(code_2)
