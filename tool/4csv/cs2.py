import pandas as pd
import os

main_dir = os.path.expanduser("~")
# 读取CSV文件
point_file = os.path.join(main_dir, "4csv", 'point.csv')

# 将CSV文件读取为DataFrame对象
point_df = pd.read_csv(point_file)

# 获取PID为1的行
pid_1_row = point_df[point_df['PID'] == 1]

# 获取最后一行的Bx和Ly的值
last_row = point_df.iloc[-1]

# 将最后一行的Bx和Ly值替换为PID为1的行的对应值
last_row['Bx'] = pid_1_row['Bx'].values[0]
last_row['Ly'] = pid_1_row['Ly'].values[0]

# 更新最后一行的值
point_df.iloc[-1] = last_row

# 将结果写回point.csv文件
point_df.to_csv(point_file, index=False)
