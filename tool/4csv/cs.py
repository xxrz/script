import os
import csv

main_dir = os.path.expanduser("~")

folder_path = os.path.join(main_dir, "4csv")
files_to_modify = ["dtlane.csv", "lane.csv"]

# 删除文件夹中指定文件的最后一行
for file_name in files_to_modify:
    file_path = os.path.join(folder_path, file_name)
    with open(file_path, "r") as file:
        lines = file.readlines()
    with open(file_path, "w") as file:
        file.writelines(lines[:-1])

# 修改 lane.csv 的最后一行 FLID 数值为 0
lane_file_path = os.path.join(folder_path, "lane.csv")
with open(lane_file_path, "r") as file:
    lines = file.readlines()
last_line_values = lines[-1].strip().split(",")  # 获取最后一行的值并去除换行符
last_line_values[3] = "1"  # 将最后一行的第4个元素（即 FLID）改为 0
lines[-1] = ",".join(last_line_values) + "\n"  # 将修改后的最后一行重新拼接为字符串并添加换行符
with open(lane_file_path, "w") as file:
    file.writelines(lines)

