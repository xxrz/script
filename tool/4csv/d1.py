import pandas as pd
import os

# 获取脚本所在的文件夹路径
main_dir = os.path.expanduser("~")

# 读取point.csv文件
point_file = os.path.join(main_dir, '4csv', 'point.csv')
point_df = pd.read_csv(point_file)

# 获取每20行的最后一行的index
last_rows = point_df.groupby(point_df.index // 20 * 20).tail(1).index.tolist()

# 初始化dtlane的DataFrame
dtlane_df = pd.DataFrame(columns=['DID', 'Dist', 'PID', 'Dir', 'Apara', 'r', 'slope', 'cant', 'LW', 'RW'])

# 初始化变量用于存储当前段的第一个有效的Ly值
first_ly_in_segment = point_df.iloc[0]['Ly']

# 遍历point.csv中的每一行
for index, row in point_df.iterrows():
    # 计算Dist值
    if index in last_rows:
        first_ly_in_segment = row['Ly']
    dist = abs(row['Ly'] - first_ly_in_segment)
    
    # 添加新行到dtlane_df
    dtlane_df = dtlane_df.append({
        'DID': index + 1,
        'Dist': dist,
        'PID': index + 1,
        'Dir': 0,
        'Apara': 0,
        'r': 0,
        'slope': 0,
        'cant': 0,
        'LW': 0,
        'RW': 0
    }, ignore_index=True)

# 保留小数点后两位
dtlane_df = dtlane_df.round({'Dist': 2})

# 保存dtlane_df到CSV文件
dtlane_file = os.path.join(main_dir, '4csv', 'dtlane.csv')
dtlane_df.to_csv(dtlane_file, index=False)

