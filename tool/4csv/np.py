import csv
import os

main_dir = os.path.expanduser("~")

input_csv_path = os.path.join(main_dir, 'saved_waypoints.csv')
output_folder_path = os.path.join(main_dir, '4csv')

if not os.path.exists(output_folder_path):
    os.makedirs(output_folder_path)

output_csv_path = os.path.join(output_folder_path, 'point.csv')
node_csv_path = os.path.join(output_folder_path, 'node.csv')

# 读取原始CSV文件中的x和y值，并保留小数点后两位
with open(input_csv_path, mode='r') as infile:
    reader = csv.reader(infile)
    next(reader, None)  # 跳过标题行
    xy_values = [("{:.2f}".format(float(row[0])), "{:.2f}".format(float(row[1])) ) for row in reader]

# 打开或创建新的CSV文件进行写入
with open(output_csv_path, mode='w', newline='') as outfile, \
     open(node_csv_path, mode='w', newline='') as nodefile:  # 同时打开node.csv文件
    writer = csv.writer(outfile)
    node_writer = csv.writer(nodefile)  # 创建node.csv文件的writer
    
    # 写入标题行
    writer.writerow(['PID', 'B', 'L', 'H', 'Bx', 'Ly', 'ReF', 'MCODE1', 'MCODE2', 'MCODE3'])
    node_writer.writerow(['NID', 'PID'])  # 写入node.csv的标题行
    
    # 写入数据行，并在PID为20的倍数时，在其后复制一行
    pid = 1
    for x, y in xy_values:
        writer.writerow([pid, 0, 0, 0, y, x, 0, 0, 0, 0])
        node_writer.writerow([pid, pid])  # 写入node.csv的数据行
        
        # 检查PID是否是20的倍数
        if pid % 20 == 0:
            # 如果是，则复制当前行，但PID需要递增
            pid += 1
            writer.writerow([pid, 0, 0, 0, y, x, 0, 0, 0, 0])
            node_writer.writerow([pid, pid])  # 写入node.csv的复制行
        
        # PID递增
        pid += 1

