import pandas as pd
import os

# 获取脚本所在的文件夹路径
main_dir = os.path.expanduser("~")

# 读取数据文件
data_file = os.path.join(main_dir, '4csv', 'dtlane.csv')
data = pd.read_csv(data_file)

# 删除重复的连续0值行
data = data.loc[(data['Dist'] != 0) | (data['Dist'].shift(1) != 0)]
data['DID'] = range(1, len(data) + 1)
# 重新索引行号
data.reset_index(drop=True, inplace=True)

# 初始化记录上一个为0的索引，初始设置为-1，因为Python索引从0开始
last_zero_index = -1
# 初始化计数器用于记录两个0之间非0的数量
non_zero_count = 0

# 更新符合条件的 PID
for i in range(1, len(data)):
    if data.loc[i, 'DID'] >= 10:
        # 当前Dist为0时的处理
        if data.loc[i, 'Dist'] == 0:
            # 如果上一个0的索引为-1（即这是第一个0）或者两个0之间非0的数量大于等于10
            if last_zero_index == -1 or non_zero_count >= 10:
                data.loc[i, 'PID'] = data.loc[i-1, 'PID'] + 2
                # 重置非0计数器
                non_zero_count = 0
            else:
                # 如果两个0之间非0的数量小于10，则仅增加1
                data.loc[i, 'PID'] = data.loc[i-1, 'PID'] + 1
            # 更新上一个为0的索引
            last_zero_index = i
        # 当前Dist非0时的处理
        else:
            # 非0的情况下直接增加1
            data.loc[i, 'PID'] = data.loc[i-1, 'PID'] + 1
            # 增加非0计数器
            non_zero_count += 1
# 重新进行序列编号
data['DID'] = range(1, len(data) + 1)

# 保存修改后的数据到新文件
updated_data_file = os.path.join(main_dir, '4csv', 'dtlane.csv')
data.to_csv(updated_data_file, index=False)

