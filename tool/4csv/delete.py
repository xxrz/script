import pandas as pd
import os

# 获取主文件夹路径
main_dir = os.path.expanduser("~")
# CSV文件路径
csv_folder = os.path.join(main_dir, "4csv")
csv_files = ['dtlane.csv', 'lane.csv', 'node.csv', 'point.csv']

# 循环处理每个CSV文件
for file in csv_files:
    file_path = os.path.join(csv_folder, file)
    # 读取CSV文件
    df = pd.read_csv(file_path)
    # 删除最后一行数据
    # df = df[:-1]
    # 将单元格中的0.0替换为0
    df = df.applymap(lambda x: 0 if x == 0.0 else x)
    # 写回CSV文件
    df.to_csv(file_path, index=False)
