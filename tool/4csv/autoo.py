# import tkinter as tk
# from tkinter import messagebox
import sys
import os
import subprocess

def run_script(script):
    # 获取临时目录路径
    #temp_dir = getattr(sys, '_MEIPASS', os.path.abspath('.'))
    main_dir = os.path.expanduser("~")

    # 拼接脚本文件路径
    script_path = os.path.join(main_dir,"./.sh/script/tool/4csv/", script)

    # 使用subprocess模块执行启动命令
    subprocess.call(['python3', script_path])

def show_popup():
    # root = tk.Tk()
    # root.withdraw()  # 隐藏根窗口

    # messagebox.showinfo("注意", "请将saved_waypoints.csv放到主目录下，然后生成的文件，请查看主目录的4csv文件")

    # 指定要启动的脚本文件名
    scripts = ['np.py', 'd1.py', 'd2.py', 'l.py', 'cs.py', 'cs1.py', 'cs2.py',]

    # 逐个运行脚本
    for script in scripts:
        run_script(script)

show_popup()

