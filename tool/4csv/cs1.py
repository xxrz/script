import pandas as pd
import os

main_dir = os.path.expanduser("~")
# 读取CSV文件
lane_file = os.path.join(main_dir, "4csv", 'lane.csv')
node_file = os.path.join(main_dir, "4csv", 'node.csv')
point_file = os.path.join(main_dir, "4csv", 'point.csv')

# 将CSV文件读取为DataFrame对象
lane_df = pd.read_csv(lane_file)
node_df = pd.read_csv(node_file)
point_df = pd.read_csv(point_file)

# 获取lane.csv中FNID的最大值
max_fnid = lane_df['FNID'].max()

# 获取node.csv中PID的最大值
max_pid = node_df['PID'].max()

# 获取node.csv中PID的最大值
max_ppid = point_df['PID'].max()

# 比较并根据条件删除node.csv中的行
if max_pid > max_fnid:
    # 只保留PID小于等于max_fnid的行
    filtered_node_df = node_df[node_df['PID'] <= max_fnid]
    # 将结果写回node.csv文件
    filtered_node_df.to_csv(node_file, index=False)

# 比较并根据条件删除node.csv中的行
if max_ppid > max_fnid:
    # 只保留PID小于等于max_fnid的行
    filtered_node_pdf = point_df[point_df['PID'] <= max_fnid]
    # 将结果写回node.csv文件
    filtered_node_pdf.to_csv(point_file, index=False)
