# 打包
pyinstaller auto.py --onefile --add-data "/home/djyx/Music/R/4csv/*:." -p "/home/djyx/Music/R/4csv" --distpath /home/djyx/auto_djyx_contest/2/


## 以下生成路线文件可直接在程序加载出来。需要修改路线的话，请使用windows系统下的Unity软件，文件不能直接加载进去，由于路径文件格式不对，导致在Unity软件无法直接加载出来，解决办法是，在电脑上打开这4个路径软件，用wps或者表格打开，不能用记事本打开，然后按ctrl+s保存文件，会弹出格式什么什么的，选择是，然后在Unity软件加载即可

# 可生成不回环路线
autoc.py

# 可生成回环路线
autoo.py






