import csv
import os

main_dir = os.path.expanduser("~")

input_file = os.path.join(main_dir, '4csv', 'dtlane.csv')
output_file = os.path.join(main_dir, '4csv', 'lane.csv')
fnid = 2  # 初始化FNID为2

with open(input_file, 'r') as file:
    reader = csv.DictReader(file)
    lanes = list(reader)

fieldnames = ['LnID', 'DID', 'BLID', 'FLID', 'BNID', 'FNID', 'JCT',
              'BLID2', 'BLID3', 'BLID4', 'FLID2', 'FLID3', 'FLID4',
              'ClossID', 'Span', 'LCnt', 'Lno', 'LaneType',
              'LimitVel', 'RefVel', 'RoadSecID', 'LaneChgFG']

with open(output_file, 'w', newline='') as file:
    writer = csv.DictWriter(file, fieldnames=fieldnames)
    writer.writeheader()

    pid = 2  # 初始化pid为2

    for lane in lanes:
        did = int(lane['DID'])
        pid = int(float(lane['PID']))

        row = {
            'LnID': did,
            'DID': did,
            'BLID': did - 1,
            'FLID': did + 1,
            'BNID': pid,  # 复制PID列到BNID列
            'FNID': fnid,  # 更新FNID列
            'JCT': 0,
            'BLID2': 0,
            'BLID3': 0,
            'BLID4': 0,
            'FLID2': 0,
            'FLID3': 0,
            'FLID4': 0,
            'ClossID': 0,
            'Span': 1,
            'LCnt': 1,
            'Lno': 0,
            'LaneType': 20,
            'LimitVel': 20,
            'RefVel': 0,
            'RoadSecID': 0,
            'LaneChgFG': 0
        }

        writer.writerow(row)

        # 更新pid的值
        pid += 2
        if pid > 80:
            pid = 2

        # 更新fnid的值
        if fnid % 20 == 0:
            fnid += 2
        else:
            fnid += 1

print('lane.csv生成成功！')
