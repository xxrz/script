#!/bin/bash

source /home/$USER/auto_djyx_tractor/install/setup.sh

gnome-terminal --tab -- bash -c "roscore" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch lslidar_driver lslidar_c16.launch"
wait
exit 0
