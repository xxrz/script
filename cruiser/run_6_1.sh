#!/bin/bash

source /home/$USER/auto_djyx_cruiser/install/setup.sh

gnome-terminal --tab -- bash -c "roscore" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch runtime_manager runtime_manager.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch lslidar_driver lslidar_c16.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins map.launch" 
sleep 10
gnome-terminal --tab -- bash -c "roslaunch ins tf_map.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization.launch" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins ndt_localization_lx.launch" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins local_lane.launch" 
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins lidar_and_tracker.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins checker.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch data_interaction data_interaction.launch"
sleep 5
gnome-terminal --tab -- bash -c "roslaunch ins controller.launch"
sleep 5
gnome-terminal --tab -- bash -c "bash /home/$USER/Webcam/run.sh"
wait
exit 0
